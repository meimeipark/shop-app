import 'package:shop_app/model/things_item.dart';

class ThingsListResult {
  String msg;
  num code;
  List<ThingsItem> list;
  num totalCount;

  // 생성자 ThingsListResult
  ThingsListResult(this.msg, this.code, this.list, this.totalCount);

  // factiory (자동으로 찍어낸)
  factory ThingsListResult.fromJson(Map<String, dynamic> json) {
    return ThingsListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
          (json['list'] as List).map((e) => ThingsItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
    );
    // 람다식(lambda)
    // json으로 [{name:'ghd'}, {name:'rla'}]이런 string을 받아서 Object의 List로 바꾸고 난 후에
    // -> List를 한 뭉텅이로 만들어준다.(json['list'] as List)
    // -> List니까 한 뭉텅이씩 던져줄 수 있음.
    // map에서 핀셋을 하나씩 수거해서 ThingsItem.fromJson이 동작한다. map((e) => ThingsItem.fromJson(e)
    // -> map이라는 것 자체가 for문을 포함한다. 있는 만큼 반복하기 때문에
    // -> 한 뭉텅이씩 던지는 한 뭉텅이를 부르는 이름이 바로 e
    // 다 작은 그릇으로 바꿔치기 한 다음 다시 그것들을 싹 가져와서 List로 정리하여 쌓는다.
    // -> List를 쌓는게 toList()
  }
}