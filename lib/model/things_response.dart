class ThingsResponse {
  num id;
  String thumbNail;
  String name;
  num price;

  ThingsResponse(this.id, this.name, this.thumbNail, this.price);

  factory ThingsResponse.fromJson(Map<String, dynamic> json){
    return ThingsResponse(
        json['id'],
        json['name'],
        json['thumbNail'],
        json['price']
    );
  }
}