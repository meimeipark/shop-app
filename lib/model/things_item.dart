class ThingsItem{
  num id;
  String name;
  String thumbNail;
  num price;

  ThingsItem(this.id, this.name, this.thumbNail, this.price);

  // fromJson(메서드의 이름)은 json으로 받아서 ThingsItem으로 바꿔(변환해)준다.
  // -> ThingsItem(return 타입).fromJson(메서드의 이름)
  // 생성자 호출 ThingsItem(this.id, this.name, this.thumbNail, this.price)
  // 위치가 바뀔 수 도 있기 때문에 마지막까지 항상 , 찍어주기(다트권장)
  factory ThingsItem.fromJson(Map<String, dynamic> json) {
    return ThingsItem(
      json['id'],
      json['name'],
      json['thumbNail'],
      json['price'],
    );
  }
}