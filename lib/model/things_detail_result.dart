import 'package:shop_app/model/things_response.dart';

class ThingsDetailResult {
  String msg;
  num code;
  ThingsResponse data;

  ThingsDetailResult(this.msg, this.code, this.data);

  factory ThingsDetailResult.fromJson(Map<String, dynamic> json){
    return ThingsDetailResult(
        json['msg'],
        json['code'],
        ThingsResponse.fromJson(json),
    );
  }
}