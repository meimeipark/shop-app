import 'package:flutter/material.dart';
import 'package:shop_app/model/things_item.dart';

// 한번 설정 하면 바꿀 일이 없기 때문에 stateless
class ComponentThingsItem extends StatelessWidget {
  const ComponentThingsItem({
    super.key,
    required this.thingsItem,
    required this.callback
  });

  final ThingsItem thingsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Text(thingsItem.thumbNail),
            Text(thingsItem.name),
            Text('${thingsItem.price}원'),
          ],
        ),
      ),
    );
  }
}
