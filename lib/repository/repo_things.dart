// repository 파일 명은 front(swagger-ui:'v1/things') 기준으로 생성
// 프론트는 기본적으로 비동기(여러사람이 나눠서 작업)

import 'package:dio/dio.dart';
import 'package:shop_app/config/config_api.dart';
import 'package:shop_app/model/things_list_result.dart';
import 'package:shop_app/model/things_detail_result.dart';


// Future<ThingsListResult> 다음 method 작성
// method의 prifix는 list를 가져오는 것이기 때문에 get을 사용
// 요청하는 파라미터가 없기 때문에 ()으로 설정 -> 아무것도 주지 않는다.
// 타입이 없는 언어에서는 async await
// 타입이 존재하는 컴파일 언어에서는 Futer async

class RepoThings {
  // 복수 R
  Future<ThingsListResult> getThings() async {
    // Dio 객체 타입 생성 후 Dio() 빈 생성자 호출
    // dio 사용 시 pub.dev에서 installing
    Dio dio = Dio();

    // const와 final차이점
    // const: 콘크리트(넣고 바로 붙여줘야 함)
    // final: 처음에는 비어있다 올 때까지 기다린다.
    final String _baseUrl = '$apiUri/things/all'; // 엔드포인트

    // api로 받은 값이 변경되면 안되기 때문에 상수처리가 필요하나,
    // final로 올때까지 기다려야 한다.
    // CORS에러 발생 시 backend에서 처리가 필요
    final response = await dio.get(
        _baseUrl,
        options: Options(
          followRedirects: false,
          // 통신연결 상태
          validateStatus: (status) {
            // 200 => 성공
            // 성공 했을 때만 return
            return status == 200;
          }
      ),
    );
    return ThingsListResult.fromJson(response.data);
  }

//   단수R
  Future<ThingsDetailResult> getThing(num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/things/detail/{id}';

    final response = await dio.get(
      // 찾아서 바꿔줘 replaceAll
      _baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );
    return ThingsDetailResult.fromJson(response.data);
  }
}