// 디자인 요소가 들어 갔기 때문에 material 사용
import 'package:flutter/material.dart';
import 'package:shop_app/components/component_things_item.dart';
import 'package:shop_app/model/things_item.dart';
import 'package:shop_app/pages/page_things_detail.dart';
import 'package:shop_app/repository/repo_things.dart';

// 라이프 사이클 탄생 -> 부착 -> 수정 -> 파괴(소멸)
// 탄생: 데이터를 줌
// 부착: 화면으로 보여줌(소품을 장착하고)
// hook 때문에 stful을 사용해야한다.
class PageThingsList extends StatefulWidget {
  const PageThingsList({super.key});

  @override
  State<PageThingsList> createState() => _PageThingsListState();
}

class _PageThingsListState extends State<PageThingsList> {
  List<ThingsItem> _list = [];


  // vue 참고
  // 미래에 오긴 하는데 기다려야하기 때문에 void
  Future<void> _loadList() async {
    // 이 메서드가 repo를 호출해서 데이터를 받아온 다음에
    // setstate()해서 _list 교체할 것이다.
    // 동기식으로 일해야한다.
    await RepoThings().getThings()
        .then((res) => {
          print(res),
          setState(() {
            _list = res.list;
          })
        })
        .catchError((err) => {
          debugPrint(err)
        });
  }


  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '상품 리스트'
        ),
      ),
      //required Widget? Function(BuildContext, int) itemBuilder,
      // 벽돌을 나르기 위해서 몇 번 나를 건지를 설정해줘야한다.
      body: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int idx) {
          return ComponentThingsItem(
              thingsItem: _list[idx],
              callback: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageThingsDetail(id: _list[idx].id)));
          });
        },
      ),
    );
  }
}
