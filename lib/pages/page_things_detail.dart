import 'package:flutter/material.dart';
import 'package:shop_app/model/things_response.dart';
import 'package:shop_app/repository/repo_things.dart';

// id값 가지고 넘겨야 하기 때문에 stateful -> initstate
class PageThingsDetail extends StatefulWidget {
  const PageThingsDetail({
    super.key,
    required this.id
  });
  
  final num id;

  @override
  State<PageThingsDetail> createState() => _PageThingsDetailState();
}

class _PageThingsDetailState extends State<PageThingsDetail> {

  ThingsResponse? _detail;

  Future<void> _loadDetail() async {
    await RepoThings().getThing(widget.id)

        .then((res) => {
          setState(() {
            _detail = res.data;
          })
    });
  }

  @override
  void initState(){
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('제품 상세보기'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context){
    // 스켈레톤UI
    if(_detail == null) {
      return Text('데이터로딩중');
    } else {
      return ListView(
        children: [
          Text('${widget.id}'),
          Text(_detail!.thumbNail),
          Text(_detail!.name),
          Text('${_detail!.price}'),
        ],
      );
    }
  }
}
